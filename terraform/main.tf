terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "us-west-2"
}


data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_subnet_ids" "subs" {
  vpc_id = var.vpc_id
}