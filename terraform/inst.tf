resource "aws_security_group" "ubuntu_instance"{
    name_prefix = var.name
    vpc_id = var.vpc_id
}

resource "aws_instance" "app_server" {
  ami           = "ami-0d70546e43a941d70"
  instance_type = var.instance_type
  security_groups    = [aws_security_group.ubuntu_instance.name]
  key_name = var.mysshkey
  tags = {
    Name = "ubuntu-webserver"
  }
}