
resource "aws_security_group_rule" "allow_ubuntu_in_80"{
    security_group_id = aws_security_group.ubuntu_instance.id
    type = "ingress"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    source_security_group_id = aws_security_group.this_alb.id 
}
resource "aws_security_group_rule" "allow_ubuntu_in_22"{
    security_group_id = aws_security_group.ubuntu_instance.id
    type = "ingress"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
}
resource "aws_security_group_rule" "allow_ubuntu_out"{
    security_group_id = aws_security_group.ubuntu_instance.id
    type = "egress"
    from_port = 0
    to_port = 65535
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group" "this_alb"{
    name_prefix = var.name
    vpc_id = var.vpc_id
}
resource "aws_security_group_rule" "allow_alb_in_80"{
    security_group_id = aws_security_group.this_alb.id
    type = "ingress"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
}
resource "aws_security_group_rule" "allow_alb_out"{
    security_group_id = aws_security_group.this_alb.id
    type = "egress"
    from_port = 0
    to_port = 65535
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
}
resource "aws_security_group_rule" "allow_alb_in_443"{
    security_group_id = aws_security_group.this_alb.id
    type = "ingress"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
}
